// DI at a root module level
@NgModule({
    provides: [Service]
 })
export class AppModule {...}
  
// DI at any component level, for example at AppComponent level
@Component({
    providers: [Service]
})
export class AppComponent {...}