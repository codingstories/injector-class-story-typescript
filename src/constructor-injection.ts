@Component({
  selector: 'app-summary-page',
  templateUrl: './summary-page.component.html',
  styleUrls: ['./summary-page.component.scss']
})
export class SummaryPageComponent implements OnInit, OnDestroy {
  constructor(private injector: Injector) {
    this.componentFactoryResolver = injector.get<ComponentFactoryResolver>(ComponentFactoryResolver);
    this.companyTaxSetupService = injector.get<CompanyTaxSetupStateService>(CompanyTaxSetupStateService);
    this.companyTaxSetupCommonService = injector.get<CompanyTaxSetupStateCommonService>(CompanyTaxSetupStateCommonService);
    this.translateService = injector.get<UkgTranslateService>(UkgTranslateService);
    this.changeDetectorRef = injector.get<ChangeDetectorRef>(ChangeDetectorRef);
    this.codeTablesService = injector.get<UkgCodeTablesService>(UkgCodeTablesService);
    this.companyTaxSetupSharedCommonService = injector.get<CompanyTaxSetupCommonService>(CompanyTaxSetupCommonService);
    this.companyTaxSetupTranslationService = injector.get<CompanyTaxSetupStateTranslationService>(CompanyTaxSetupStateTranslationService);
  } {}
  
  ngOnDestroy(): void {}

  ngOnInit(): void {}
}
 