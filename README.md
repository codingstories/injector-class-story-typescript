# Injector class to address code smell issue

**To read**: [https://gitlab.com/coding-stories2/injector-class-story-typescript.git]

**Estimated reading time**: 10 mins

## Story Outline

In this coding story we will discuss how to handle "Constructor has too many parameters (8). Maximum allowed is 7." code smell.
To address the above issue we will be using Angular code module Injector class as one of the best approach.

## Story Organization

**Story Branch**: main

> `git checkout main`

Tags: #angular, #typescript, #static-code-analysis, #sonarqube, #code-smell
